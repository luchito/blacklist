<%@ page title="" language="C#" masterpagefile="~/Template/SIEMasterPage.master" autoeventwireup="true" inherits="BlackListBusqueda, App_Web_vroqhnfz" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainSup" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCentro" Runat="Server">
     <script type="text/javascript" >
         
     </script>
     <h3 class="titulo-contenido-bl">
        Permisos de internet</h3>
     <hr />
     <div class="subtitulo-contenido-bl">
        
            Gestioná que lineas de la cuenta pueden  comprar internet luego de haber alcanzado el límite de 
            su plan.
        <hr class="hr-bl" size="20"/>
     <input id="sessionInput" type="hidden" value='<%= Session["UserId"] %>' />
     </div>
    <div class="sie-form">
        <h3 class="titulo-contenido">
        Buscar</h3>
            <div class="line-form fdos">
                <div class="cuenta-mail cuenta-mail-azul">
                    <label for="cuenta" class="label-bl">
                        Seleccioná tu cuenta
                    </label>
                    <span class="label-bl">ó</p>
                    <label for="linea" class="label-bl">
                        Ingresá una línea
                    </label>
                </div>
                /* se agrego la clase select-bl*/
                <div class="select-cuenta-mail select-bl">
                    <asp:DropDownList ID="ddlCuenta" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCuenta_SelectedIndexChanged" 
                        ClientIDMode="Static" Height="19px" Placeholder="Seleccioná una cuenta">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtNroLinea" runat="server" onkeypress="javascript:return Validar_SoloNumeros(event);" MaxLength="13"
                        ClientIDMode="Static" Placeholder="Ingresá una linea" Height="16px">
                    </asp:TextBox>
                </div> 
                
                <div class="boton-cuenta-mail">
                    /* actualizar este boton se ubico dentro del div con class btn, se elimino la clase botoncyan y se agrego la clase buscar-bl */
                    <div class="boton-izq-filtro ">
                        <div class="btn">
                        <%--<asp:Button ID="btnVerLinea" runat="server" Text="Buscar" CssClass="botoncyan-a bot-input input1" 
                            OnClick="btnVerLinea_Click" />--%>
                            <%--<div class="btn-blacklist">
                            <asp:Button ID="btnVerLinea" runat="server" Text="Buscar"
                            OnClick="btnVerLinea_Click" />
                    </div>  --%>                                                  
                            <asp:Button ID="btnVerLinea" runat="server" Text="Buscar" CssClass=" bot-input input1 buscar-bl" 
                            OnClick="btnVerLinea_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- SELECCION SUBCUENTAS-->
                <asp:Panel ID="pnlSubcuentas" runat="server" Visible="false">
                    <div class="line-form funo">
                        <label for="linea" id="subcuentas" class="label-bl">
                        Ingresá una subcuenta
                        </label>
                        <div class="select-cuenta-mail select-bl">
                        <asp:DropDownList ID="ddlSubCuentaNivel1" runat="server" CssClass="large-s" TabIndex="1"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubCuentaNivel1_SelectedIndexChanged" ClientIDMode="Static">
                        </asp:DropDownList>
                        </div>
                    </div>
                    <div class="line-form funo">
                        <asp:DropDownList ID="ddlSubCuentaNivel2" runat="server" CssClass="large-s" TabIndex="2"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubCuentaNivel2_SelectedIndexChanged" ClientIDMode="Static">
                        </asp:DropDownList>
                    </div>
                    <div class="line-form funo">
                        <asp:DropDownList ID="ddlSubCuentaNivel3" runat="server" CssClass="large-s" TabIndex="3"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlSubCuentaNivel3_SelectedIndexChanged" ClientIDMode="Static">
                        </asp:DropDownList>
                    </div>
                </asp:Panel>
            <asp:Panel ID="pnlResultadoBusqueda" runat="server" CssClass="line-form fdos" Visible="false">
                <div class="cuenta-mail cuenta-mail-azul">
                    <h3 class="titulo-contenido">
                        Resultado de búsqueda</h3>                                                   
                </div>
                
                /* actualizar este boton se agrego el class btn, se elimino la clase botoncyan y se agrego la clase .blacklist .puede .no-puede en cada boton respectivo*/
                <div class="boton-cuenta-mail">
                    
                        <div class="tabla-contenido-100">
                            <asp:HiddenField ID="hHabilitado" runat="server" />
                            <asp:HiddenField ID="hDeshabilitado" runat="server" />
                            <div class="botones-centrados" id="divBotones" runat="server" >
                            <%--<asp:CheckBox ID="chkTodo" runat="server" Text="Todos" />--%>
                                <div class="btn"> 
                                <asp:Button ID="btnDeshabilitar" runat="server" Text="No Puede Comprar" CssClass="blacklist no-puede bot-input input1"
                                    OnClick="btnDeshabilitar_Click"/>
                                </div>
                                <div class="btn">
                                <a href="#">No Puede Comprar en DIV </a>
                                <asp:Button ID="btnHabilitar" runat="server" Text=" Puede Comprar" CssClass="blacklist puede bot-input input1"
                                    OnClick="btnHabilitar_Click"/>
                                </div>
                            </div>                                
                                    <div class="combo-filtro-derecha">                                        
                                        <asp:DropDownList ID="cboFiltroEstado" runat="server" 
                                            onselectedindexchanged="cboFiltroEstado_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="-1" Text="Mostrar:"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Todo"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Puede comprar"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="No puede comprar"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>                                                        
                        </div>
                    
                </div>
                <div class="tabla-contenido">
                    
                    <asp:GridView ID="grdLineas" runat="server" CssClass="tb-page" AutoGenerateColumns="False" PageSize="10"
                        AllowPaging="True" OnRowCommand="grdLineas_RowCommand" OnPageIndexChanging="grdLineas_PageIndexChanging" 
                        OnRowDataBound="grdLineas_RowDataBound" DataKeyNames="Linea">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSeleccionarTodos" runat="server" OnCheckedChanged="chkSeleccionarTodos_CheckedChanged" 
                                        AutoPostBack ="true" />                                        
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:BoundField DataField="Linea" HeaderText="L&iacute;nea" />
                            <asp:TemplateField HeaderText="¿Puede comprar?" >
                                <ItemTemplate>
                                    <asp:Image ID="imgBlackList" runat="server" />
                                    <asp:Label Visible ="false" ID="lblBlackList" runat="server" Text='<%#(Boolean.Parse(Eval("BlackList").ToString())) ? "x" : "&#10003;"%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            
                            
                        </Columns>
                        <HeaderStyle CssClass="head-grid" />
                        <RowStyle CssClass="datos" />
                        <AlternatingRowStyle BackColor="#EEEEEE" />
                        
                    </asp:GridView>
                </div>
            </asp:Panel>
            
                    
                
            <asp:Panel ID="pnlMessage" runat="server" CssClass="line-form-l" Visible="false">
                    <asp:Label ID="lblMsg" runat="server" CssClass="error"></asp:Label>
                </asp:Panel>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainInf" Runat="Server">
</asp:Content>

